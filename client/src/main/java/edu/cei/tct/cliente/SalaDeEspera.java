package edu.cei.tct.cliente;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

import edu.cei.tct.cliente.Window;
import edu.cei.tct.common.comando.Comando;
import edu.cei.tct.common.usuario.Usuario;
import edu.cei.tct.cliente.ClienteImpl;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;

public class SalaDeEspera extends JPanel implements Window {

	private DefaultListModel<Usuario> usuarios;
	private JList<Usuario> list;
	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SalaDeEspera window = new SalaDeEspera();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public SalaDeEspera() throws RemoteException {
		ClienteImpl.getInstance().registrarCliente();
		initialize();
	}

	private void initialize() {
		SpringLayout springLayout = new SpringLayout();
		setLayout(springLayout);

		list = new JList<Usuario>();
 		//scrollPane.setViewportView(list);
		
 		list.setModel(usuarios);
		JButton btnIngresar = new JButton("Buscar Partida");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.dispose();
			}
		});
		springLayout.putConstraint(SpringLayout.SOUTH, btnIngresar, -80, SpringLayout.SOUTH, this);
		springLayout.putConstraint(SpringLayout.EAST, btnIngresar, -30, SpringLayout.EAST, this);
		frame.getContentPane();
		frame.getContentPane().add(btnIngresar);

		JList list = new JList();
		springLayout.putConstraint(SpringLayout.NORTH, list, 66, SpringLayout.NORTH, this);
		springLayout.putConstraint(SpringLayout.WEST, list, 76, SpringLayout.WEST, this);
		add(list);

		JLabel label = new JLabel("");
		springLayout.putConstraint(SpringLayout.NORTH, label, 22, SpringLayout.SOUTH, btnIngresar);
		springLayout.putConstraint(SpringLayout.EAST, label, -85, SpringLayout.EAST, this);
		add(label);

	}

	//@Override
	public void mostrarMensaje(String mensaje) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mostrar() {
		this.frame.setVisible(true);
	}

	@Override
	public void close() {

	}

	@Override
	public void recibirComando(Comando comando) {
		
	}

}
