package edu.cei.tct.cliente.login;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import edu.cei.tct.cliente.ClienteImpl;
import edu.cei.tct.cliente.Estado;
import edu.cei.tct.cliente.Window;
import edu.cei.tct.cliente.wfm.WindowFlowManager;
import edu.cei.tct.common.comando.Comando;
import edu.cei.tct.common.usuario.Usuario;

import javax.swing.SpringLayout;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.awt.event.ActionEvent;

public class LoginWindow extends JFrame implements Window {

	private JPasswordField txtPassword;
	private static JFrame frame;
	private JPanel contentPane;
	private JTextField txtUsername;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginWindow loginWindow = new LoginWindow();
					loginWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public LoginWindow() {
		initialize();
	}

	/**
	 * Create the frame.
	 */
	public void initialize() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);

		txtUsername = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, txtUsername, 69, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, txtUsername, 126, SpringLayout.WEST, contentPane);
		contentPane.add(txtUsername);
		txtUsername.setColumns(10);

		txtPassword = new JPasswordField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, txtPassword, 32, SpringLayout.SOUTH, txtUsername);
		sl_contentPane.putConstraint(SpringLayout.WEST, txtPassword, 0, SpringLayout.WEST, txtUsername);
		sl_contentPane.putConstraint(SpringLayout.EAST, txtPassword, -5, SpringLayout.EAST, txtUsername);
		contentPane.add(txtPassword);

		JLabel lblNombre = new JLabel("nombre");
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNombre, 26, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblNombre, 0, SpringLayout.SOUTH, txtUsername);
		contentPane.add(lblNombre);

		JLabel lblContrasea = new JLabel("contraseña");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblContrasea, 27, SpringLayout.SOUTH, lblNombre);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblContrasea, 0, SpringLayout.WEST, lblNombre);
		contentPane.add(lblContrasea);

		JButton btnLogin = new JButton("Ingresar");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnLogin, -10, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnLogin, -10, SpringLayout.EAST, contentPane);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (getTxtUsername().equals("") || getTxtPassword().equals("")) {
					newMessageDialog("Debes completar los campos para iniciar sesion");

				} else {
					Usuario usuario = new Usuario(null, getTxtUsername(), getTxtPassword());
					try {
						Usuario usuarioRegistrado = ClienteImpl.getInstance().login(usuario);
						if (usuarioRegistrado != null) {
							if (usuarioRegistrado.getPassword().equals(getTxtPassword())) {
								WindowFlowManager.getInstance().change(Estado.SALA_DE_ESPERA);
							} else {
								newMessageDialog("Usuario y/o contraseña incorrectos, vuelva a intentar");
								System.out.println("Contraseña incorrecta");
								txtPassword.setText("");
							}
						} else {
							newMessageDialog("Usuario y/o contraseña incorrectos. Intente de nuevo");
							System.out.println("El username no existe");
							txtPassword.setText("");
						}
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}

			private void newMessageDialog(String mensaje) {
				JOptionPane.showMessageDialog(frame, mensaje, "Error", JOptionPane.INFORMATION_MESSAGE, null);
			}

		});
		contentPane.add(btnLogin);

		JButton btnNewButton = new JButton("Registrarse");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (getTxtUsername().equals("") || getTxtPassword().equals("")) {
						newMessageDialog("Debes completar los campos para registrarte");

					} else {
						Usuario usuario = new Usuario(null, getTxtUsername(), getTxtPassword());
						Usuario usuarioRegistrado = null;
						try {
							usuarioRegistrado = ClienteImpl.getInstance().register(usuario);
						} catch (Exception e) {
							newMessageDialog("No se pudo registrar el usuario");
						}
						if (usuarioRegistrado != null) {
							WindowFlowManager.getInstance().change(Estado.SALA_DE_ESPERA);
						} else {
							newMessageDialog("Usuario ya esta registrado");
							txtPassword.setText("");
						}
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

			private void newMessageDialog(String mensaje) {
				JOptionPane.showMessageDialog(frame, mensaje, "Error", JOptionPane.INFORMATION_MESSAGE, null);
			}
		});

		sl_contentPane.putConstraint(SpringLayout.NORTH, btnNewButton, 0, SpringLayout.NORTH, btnLogin);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnNewButton, -15, SpringLayout.WEST, btnLogin);
		contentPane.add(btnNewButton);

		JLabel lblLosCamposSirven = new JLabel("Los campos sirven para el registro y/o registro.");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblLosCamposSirven, 153, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, txtPassword, -12, SpringLayout.NORTH, lblLosCamposSirven);
		sl_contentPane.putConstraint(SpringLayout.EAST, lblLosCamposSirven, -72, SpringLayout.EAST, contentPane);
		contentPane.add(lblLosCamposSirven);
	}

	public String getTxtUsername() {
		return txtUsername.getText();
	}

	public String getTxtPassword() {
		return new String(txtPassword.getPassword());
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public boolean verificarUsuario(Usuario usuario) {
		boolean correcto = false;
		try {
			Usuario usuarioAux = new Usuario(null, txtUsername.getText(), new String(txtPassword.getPassword()));
			Usuario usuarioRegistrado = ClienteImpl.getInstance().login(usuarioAux);
			if (usuarioRegistrado != null) {
				correcto = true;
			} else {
				correcto = false;
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return correcto;
	}

	public void mostrarMensaje(String string) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mostrar() {
		this.frame.setVisible(true);
	}

	@Override
	public void close() {
		this.frame.dispose();
	}

	@Override
	public void recibirComando(Comando comando) {

	}
}
