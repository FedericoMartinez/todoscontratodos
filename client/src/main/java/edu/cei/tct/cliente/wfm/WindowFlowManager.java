package edu.cei.tct.cliente.wfm;

import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JFrame;

import edu.cei.tct.cliente.Estado;
import edu.cei.tct.cliente.MainWindow;
import edu.cei.tct.cliente.SalaDeEspera;
import edu.cei.tct.cliente.Window;
import edu.cei.tct.cliente.login.LoginWindow;
import edu.cei.tct.common.usuario.Usuario;

public class WindowFlowManager {
	/*private static WindowFlowManager instance;
	private Estado estado;
	private JFrame frame;
	private SwitchWindowAdapter switchWindowAdapter;
	private LoginWindow loginWindow;*/
	private static final WindowFlowManager instance = new WindowFlowManager();
	 
 	private Window currentWindow;
 
 	public static WindowFlowManager getInstance() {
 		return instance;
 	}
 
 	private WindowFlowManager() {
 	}
 	
 	public void start() {
 		LoginWindow loginWindow = new LoginWindow();
 		currentWindow = loginWindow;
 		cambiarWindowActiva();
 	}
 	public void change(Estado estado) throws RemoteException {
 		 		currentWindow.close();
 				
 		 		switch (estado) {
 		 		case LOGIN:
 		 			break;
 		 		case SALA_DE_ESPERA:
 		 			currentWindow = new SalaDeEspera();
 		 			break;
 		 		default:
 		 			break;
 		 		}
 		 
 		 		cambiarWindowActiva();
 		 	}
	
 	private void cambiarWindowActiva() {
 		EventQueue.invokeLater(new Runnable() {
 			public void run() {
 				try {
 					currentWindow.mostrar();
 				} catch (Exception e) {
 					e.printStackTrace();
 				}
 			}
 		});
 	}
		
	

}
