package edu.cei.tct.cliente;

import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import edu.cei.tct.cliente.login.LoginWindow;
import edu.cei.tct.cliente.wfm.WindowFlowManager;

public class App {

	public static void main(String[] args) throws RemoteException, NotBoundException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WindowFlowManager.getInstance().start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
