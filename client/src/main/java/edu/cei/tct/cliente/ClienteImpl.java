package edu.cei.tct.cliente;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.Queue;

import edu.cei.tct.common.comando.Comando;
import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.servidor.Servidor;
import edu.cei.tct.common.usuario.Usuario;
import edu.cei.tct.common.servidor.Window;
import edu.cei.tct.cliente.SalaDeEspera;

public class ClienteImpl extends UnicastRemoteObject implements Cliente {

	private Servidor servidor;
	private String id;
	private Queue<Comando> comandos;
	private UIProcessorThread uiProcessorThread;
	private SalaDeEspera salaDeEspera;

	private static ClienteImpl instance;

	public static ClienteImpl getInstance() {
		try {
			if (ClienteImpl.instance == null) {
				ClienteImpl.instance = new ClienteImpl();
			}
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
		return ClienteImpl.instance;
	}

	private ClienteImpl() throws RemoteException, NotBoundException {

		System.setProperty("java.security.policy", "file:///Users/fvillegas/Projects/cei/java.policy");
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());

		}
		Registry registry = LocateRegistry.getRegistry(1099);
		this.servidor = (Servidor) registry.lookup("server");
		this.id = String.valueOf(System.currentTimeMillis());
		this.servidor.registrarCliente(this, id);
		this.comandos = new LinkedList<>();
		this.uiProcessorThread = new UIProcessorThread();
		this.uiProcessorThread.start();

	}

	public void recibirComando(Comando comando) {
		this.comandos.add(comando);
	}

	@Override
	public void enviarMensaje(String mensaje) throws RemoteException {
		// TODO Auto-generated method stub

	}

	public Usuario login(Usuario usuario) throws RemoteException {
		Usuario usuarioLoggeado = this.servidor.getLoginController().login(usuario);
		return usuarioLoggeado;
	}
	/*@Override 
	public Usuario registrarUsuario(Usuario usuario) throws RemoteException {
 		Usuario usuarioRegistrado = this.servidor.getRegisterController().registrarUsuario(usuario);
		return usuarioRegistrado;
 		//this.servidor.registrarUsuario(usuario);
 	}*/
	public void registrarCliente() throws RemoteException {
				this.servidor.registrarCliente(this, id);
			}

	class UIProcessorThread extends Thread {

		@Override
		public void run() {
			super.run();
			while (true) {
				Comando comando = comandos.poll();
				System.out.println("------- Procesando ------- : " + comando);
				if (comando != null) {
					// mainWindow.mostrarMensaje(mensaje);
					System.out.println(comando.getAccion());
				} else {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Nothing to do here
						e.printStackTrace();
					}
				}
			}
		}
	}
	@Override
	public Usuario register(Usuario usuario) throws RemoteException {
		Usuario usuarioRegistrado = this.servidor.getRegisterController().register(usuario);
		return usuarioRegistrado;
	}

	/*public void registrarUsuario(Usuario usuarioRegistrado) {
		this.servidor.registrarUsuario(usuarioRegistrado);
	}*/

}
