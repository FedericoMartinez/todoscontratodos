package edu.cei.tct.cliente;

import java.awt.EventQueue;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.servidor.Window;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javax.swing.SpringLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;

public class MainWindow implements Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JFrame frame;
	private Cliente cliente;
	private JFXPanel fxPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * 
	 * @throws RemoteException
	 * @throws NotBoundException
	 */
	public MainWindow() throws RemoteException, NotBoundException {
		// Cliente que conecta con el servidor
		this.cliente = ClienteImpl.getInstance();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1000, 610);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);

		this.fxPanel = new JFXPanel();
		Parent root;
		try {
			root = FXMLLoader.load(getClass().getResource("MapScene.fxml"));
			Scene scene = new Scene(root);
			this.fxPanel.setScene(scene);
		} catch (IOException e) {
			e.printStackTrace();
		}

		springLayout.putConstraint(SpringLayout.NORTH, this.fxPanel, 40, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, this.fxPanel, 0, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(this.fxPanel);

		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					cliente.enviarMensaje(String.valueOf(System.currentTimeMillis()));
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block

					e1.printStackTrace();
				}
			}
		});
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 0, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 26, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(btnNewButton);
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public void mostrarMensaje(String mensaje) {
		JOptionPane.showMessageDialog(frame, mensaje);
	}

	public JFrame getFrame() {
		return frame;
	}
}
