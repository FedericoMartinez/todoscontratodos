package edu.cei.tct.cliente;

import edu.cei.tct.common.comando.Comando;

public interface Window {

	public void mostrar();
 	public void close();
	public void recibirComando(Comando comando);
}
