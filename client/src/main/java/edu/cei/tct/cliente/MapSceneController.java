/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.cei.tct.cliente;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

/**
 *
 */
public class MapSceneController implements Initializable {
    
	@FXML
	private Label AmericaSur;
	private Label AmericaCentral;
	private Label AmericaNorte;
	private Label Europa;
	private Label AfricaSur;
	private Label AfricaNorte;
	private Label Asia;
	private Label Rusia;
	private Label Oceania;
	
    @FXML
    private void handleButtonAction(MouseEvent event) {
        System.out.println("You clicked me!");
        System.out.print("---> ");
        System.out.println(event);
        this.AmericaSur.setText(event.getSource().toString());
        this.Rusia.setText(event.getSource().toString());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
}
