package edu.cei.tct.common.servidor.controller;

import java.rmi.RemoteException;

import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.usuario.Usuario;

public interface LoginController {
	
	public Usuario login(Usuario usuario) throws RemoteException;
	
}
