package edu.cei.tct.common.usuario;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Usuario {
	private Integer id;
	private String username;
	private String password;

	public Usuario() {
	}

	public Usuario(Integer id, String username, String password) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean equals(Object object) {
		boolean equals = false;

		if (object instanceof Usuario) {
			Usuario usuario = (Usuario) object;
			if (this.getUsername().equals(usuario.getUsername()) && this.getPassword().equals(usuario.getPassword())) {
				equals = true;
			}
		}
		return equals;
	}
}
