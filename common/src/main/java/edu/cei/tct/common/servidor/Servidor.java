package edu.cei.tct.common.servidor;

import java.rmi.Remote;
import java.rmi.RemoteException;

import edu.cei.tct.common.servidor.controller.LoginController;
import edu.cei.tct.common.usuario.Usuario;

public interface Servidor extends Remote {
	public void registrarCliente(Cliente cliente, String id) throws RemoteException;
	public LoginController getLoginController() throws RemoteException;
	/*public void registrarUsuario(Usuario usuario);*/
	public RegisterController getRegisterController() throws RemoteException;
}
