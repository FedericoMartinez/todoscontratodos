package edu.cei.tct.common.servidor;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

import edu.cei.tct.common.comando.Comando;
import edu.cei.tct.common.usuario.Usuario;


public interface Cliente extends Remote, Serializable {

	public void recibirComando(Comando comando) throws RemoteException;
	
	public void enviarMensaje(String mensaje) throws RemoteException;
	
	
	public Usuario login(Usuario usuario) throws RemoteException;

	Usuario register(Usuario usuario) throws RemoteException;

	/*Usuario registrarUsuario(Usuario usuario) throws RemoteException;
	*/
}