package edu.cei.tct.common.servidor;

import java.rmi.Remote;
import java.rmi.RemoteException;

import edu.cei.tct.common.usuario.Usuario;

public interface RegisterController extends Remote{

	public Usuario register(Usuario usuario) throws RemoteException;

}
