package edu.cei.tct.common.servidor.controller;

import java.rmi.RemoteException;

import edu.cei.tct.common.usuario.Usuario;

public interface UsuarioService {

	Usuario buscarUsuario(Usuario usuario) throws RemoteException;

	Usuario registrarUsuario(Usuario usuario);

}
