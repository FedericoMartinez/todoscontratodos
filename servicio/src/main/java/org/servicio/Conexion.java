package org.servicio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conexion {
	
	private static Connection cn;

	public static Connection getConnection() {
		try {
			
			//DESCARGAR OTRAS LIBRERIAS
			//https://thusithamabotuwana.wordpress.com/2012/07/19/connecting-to-sql-server-from-java/
			//https://www.youtube.com/watch?v=9r00K04UUWg&t=5s 
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			System.out.println("alla");
			cn = DriverManager.getConnection("jdbc:sqlserver://ASUS-TP500\\SQLEXPRESS;databaseName=war;integratedSecurity=true");
			//\\SQLEXPRESS
			//ASUS-TP500
			//localhost:1433
			//"jdbc:sqlserver://localhost:1433;databaseName=war" + "integratedSecurity=true;"
		} catch (Exception e) {
			System.out.println("aca");
			cn = null;
		}
		
		return cn;
	}
	public static void main(String[] args) throws SQLException {
		Connection pruebaCn = Conexion.getConnection();
		if (pruebaCn != null) {
			System.out.println("Conectado");
			System.out.println(pruebaCn);
			Statement stm = pruebaCn.createStatement();
			ResultSet rst = stm.executeQuery("select * from persona");
			while (rst.next()) {
				System.out.println("ID: " + rst.getInt(1) + "Nombre: " + rst.getString(2));
			}
		} else {
			System.out.println("Desconectado");
		}
	}

}
