package edu.cei.tct.servidor;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import edu.cei.tct.common.servidor.RegisterController;
import edu.cei.tct.common.usuario.Usuario;
import edu.cei.tct.service.UsuarioServiceImpl;

public class RegisterControllerImpl extends UnicastRemoteObject implements RegisterController, Serializable {

	private static RegisterControllerImpl instance;

	public static RegisterControllerImpl getInstance() throws RemoteException {
		if (instance == null) {
			instance = new RegisterControllerImpl();
		}
		return instance;
	}

	@Override
	public Usuario register(Usuario usuario) throws RemoteException {
		Usuario usuarioRegistado = UsuarioServiceImpl.getInstance().registrarUsuario(usuario);
		if(usuarioRegistado != null) {
			LoginControllerImpl.getInstance().setUsuariosEnEspera(usuarioRegistado);
		}
		return usuarioRegistado;
	}


	protected RegisterControllerImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

}
