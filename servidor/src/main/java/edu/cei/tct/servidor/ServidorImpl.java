package edu.cei.tct.servidor;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import edu.cei.tct.common.comando.Comando;
import edu.cei.tct.common.servidor.Accion;
import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.servidor.RegisterController;
import edu.cei.tct.common.servidor.Servidor;
import edu.cei.tct.common.servidor.controller.LoginController;
import edu.cei.tct.common.usuario.Usuario;

public class ServidorImpl implements Servidor {

	private static final ServidorImpl instance = new ServidorImpl();
	
	private Map<String, Cliente> clientes = new HashMap<>();

	public static ServidorImpl getInstance() {
		return instance;
	}
	
	public ServidorImpl() {
		
	}
	
	@Override
	public void registrarCliente(Cliente cliente, String id) throws RuntimeException {
		this.clientes.put(id, cliente);
		Comando comando = new Comando();
		comando.setAccion(Accion.USUARIO_LOGGEADO);
		notificar(comando);
	}

	@Override
	public LoginController getLoginController() throws RemoteException {
		return LoginControllerImpl.getInstance();
		//return LoginControllerImpl.getInstance();
	}

	public void notificar(Comando comando) {
		clientes.forEach((k, v) -> {
			notificar(v, comando);
		});
	}
	public void notificar(Cliente cliente, Comando comando) {
		try {
			cliente.recibirComando(comando);
		} catch(RemoteException e) {
			e.printStackTrace();
		}
	}
	/*public void registrarUsuario(Usuario usuario) {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public RegisterController getRegisterController() throws RemoteException {
		return RegisterControllerImpl.getInstance();
	}
}
