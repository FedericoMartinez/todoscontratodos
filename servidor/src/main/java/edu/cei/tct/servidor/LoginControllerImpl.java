package edu.cei.tct.servidor;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import edu.cei.tct.common.servidor.Cliente;
import edu.cei.tct.common.servidor.controller.LoginController;
import edu.cei.tct.common.usuario.Usuario;
import edu.cei.tct.service.UsuarioServiceImpl;

public class LoginControllerImpl extends UnicastRemoteObject implements LoginController {

	private static List<Usuario> usuariosEnEspera;
	private static LoginControllerImpl instance;
	protected LoginControllerImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Usuario login(Usuario usuario) throws RemoteException {
		return UsuarioServiceImpl.getInstance().buscarUsuario(usuario);
	}

	public static LoginControllerImpl getInstance() throws RemoteException{
		if(instance != null) {
			instance = new LoginControllerImpl();
		}
		return instance;
	}

	public void setUsuariosEnEspera(Usuario usuarioRegistado) {
		if(usuarioRegistado != null) {
			LoginControllerImpl.usuariosEnEspera.add(usuarioRegistado);
		}
	}
	
	

}
