package edu.cei.tct.service;

import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import edu.cei.tct.common.servidor.controller.UsuarioService;
import edu.cei.tct.common.usuario.Usuario;

public class UsuarioServiceImpl implements UsuarioService {

	private static final List<Usuario> usuarios = new LinkedList<>();
	private static final UsuarioServiceImpl instance = new UsuarioServiceImpl();
	private Usuario usuarioNuevo;
	
	public static UsuarioService getInstance() {
		return UsuarioServiceImpl.instance;
	}

	public UsuarioServiceImpl() {
		usuarios.add(new Usuario(1, "pepe1", "pepe1"));
		usuarios.add(new Usuario(2, "pepe2", "pepe2"));
		usuarios.add(new Usuario(3, "pepe3", "pepe3"));
		usuarios.add(new Usuario(4, "pepe4", "pepe4"));
		usuarios.add(new Usuario(5, "pepe5", "pepe5"));
		usuarios.add(new Usuario(6, "pepe6", "pepe6"));
		usuarios.add(new Usuario(7, "pepe7", "pepe7"));
	}

	public Usuario buscarUsuario(Usuario usuario) {
		Usuario usuarioRegistrado = null;

		Iterator<Usuario> iterator = usuarios.iterator();
		while (iterator.hasNext() && usuarioRegistrado == null) {
			Usuario usuarioAux = iterator.next();
			// TODO cambiar la implementacion del equals
			// Da problemas dependiendo de como se use
			if (usuarioAux.equals(usuario)) {
				usuarioRegistrado = usuarioAux;
			}
		}

		return usuarioRegistrado;
	}

	@Override
	public Usuario registrarUsuario(Usuario usuario) {
		usuarioNuevo = null;
		int id= usuarios.size() + 1;
		usuarioNuevo = new Usuario(id, usuario.getUsername(), usuario.getPassword());
		Iterator<Usuario> iterator = usuarios.iterator();
		while(iterator.hasNext() && usuarioNuevo != null) {
			Usuario usuarioAux = iterator.next();
			if(usuarioAux.equals(usuario)) {
				System.out.println("Mismo usuario");
				usuarioNuevo = null;
			}
		}
		if(usuarioNuevo != null) {
			System.out.println("Agregando...");
			usuarios.add(usuarioNuevo);
		}
		return usuarioNuevo;
	}
}
