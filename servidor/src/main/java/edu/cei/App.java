package edu.cei;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/*import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;*/

import edu.cei.tct.common.servidor.Servidor;
import edu.cei.tct.servidor.ServidorImpl;

public class App {
	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		System.setProperty("java.security.policy", "file:///Users/SCN/todoscontratodos/java.policy");
		// FEDE---> "file:///Users/Usuario/Desktop/workspace/todoscontratodos/java.policy"
		// EUGE---> "file:///Users/SCN/todoscontratodos/java.policy"
		if (System.getSecurityManager() == null) {
			System.setSecurityManager(new SecurityManager());
		}
		System.out.println("Hello World!");
		LocateRegistry.createRegistry(1099);
		Servidor srv = new ServidorImpl();
		Servidor stub = (Servidor) UnicastRemoteObject.exportObject(srv, 0);
		Registry registry = LocateRegistry.getRegistry(1099);
		registry.bind("server", stub);
		System.out.println("server ready");

	}
}
